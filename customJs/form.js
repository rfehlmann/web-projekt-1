/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {

    $.timepicker.regional['de'] = {
        timeOnlyTitle: 'Uhrzeit auswählen',
        timeText: 'Zeit',
        hourText: 'Stunde',
        minuteText: 'Minute',
        secondText: 'Sekunde',
        currentText: 'Jetzt',
        closeText: 'Auswählen',
        ampm: false
    };
    $.timepicker.setDefaults($.timepicker.regional['de']);
    $('#runUnTill').datetimepicker();
    var selectBox = $('#doneInPercent');
    for (var i = 0; i <= 10; i += 1) {
        selectBox.append(new Option(i * 10 + '%', i * 10));
    }
    listTasks();
});
function saveForm() {
    var task = {};
    task.title = $('#title').val();
    task.desc = $('#desc').val();
    task.runtTill = $('#runUnTill').val();
    task.done = $('#done').val();
    task.doneInPercent = $('#doneInPercent').val();
    appendToLocalStorage(task);
    //update grid
    jQuery("#list").empty()();
    listTasks();
    $("#result").html("Task " + task.title + " saved");
}

function appendToLocalStorage(objectToSave) {
    var savedTasks = getTasksFromStorage();
    var id = -1;
    if (savedTasks.length == 0) {
        savedTasks = new Array();
        id = 0;
    } else {
        id = savedTasks[savedTasks.length - 1].Taskid + 1;
    }

    objectToSave.Taskid = id;
    savedTasks.push(objectToSave);
    updateTask(savedTasks);
}
function updateTask(savedTasks) {
    var parsedTasks = JSON.stringify(savedTasks);
    localStorage.setItem('doToList', parsedTasks);
}

function getTasksFromStorage() {
    var savedTasksUnParsed = localStorage.getItem('doToList');
    var savedTasks = JSON.parse(savedTasksUnParsed);
    if (!savedTasks) {
        savedTasks = new Array();
    }
    return savedTasks;
}

function deleteTaskFromStorage(id) {
    var savedTasks = getTasksFromStorage();
    if (savedTasks) {
        var index = getTaskIndexFromId(id, getTasksFromStorage());
        savedTasks.splice(index, 1);
        updateTask(savedTasks);
    }
}

function listTasks() {
    var tasks = getTasksFromStorage();
    var doneInPrecentValueString = "";
    for (var n = 0; n <= 100; n += 10) {
        doneInPrecentValueString += n + ":" + n + "%;";
    }
    var lastsel2;
    jQuery("#list").jqGrid({
        datatype: "local",
        height: 250,
        colNames:
                ['Taskid', 'Title', 'Description', 'Run Till', 'Done in Percent', 'Done', 'Save', 'Delete'],
        colModel: [
            {name: 'Taskid', index: 'Taskid', width: 0, sorttype: "int", editable: false},
            {name: 'title', index: 'title', width: 150, editable: true, editoptions: {size: "20", maxlength: "30"}},
            {name: 'description', index: 'description', width: 60, editable: true, edittype: "textarea", editoptions: {rows: "2", cols: "10"}},
            {name: 'runtTill', index: 'runtTill', sorttype: "date", width: 90, editable: true, },
            {name: 'doneInPercent', index: 'doneInPercent', width: 200, sortable: false, editable: true, edittype: "select", editoptions: {value: doneInPrecentValueString}},
            {name: 'done', index: 'done', width: 60, editable: true, edittype: "checkbox", editoptions: {value: "1:0"}, formatter: "checkbox"},
            {name: 'save', index: 'save', width: 75, sortable: false},
            {name: 'delete', index: 'delete', width: 75, sortable: false},
        ],
        onSelectRow: function(id) {
            if (id && id !== lastsel2) {
                jQuery('#list').jqGrid('restoreRow', lastsel2);

                lastsel2 = id;
            }
        },
        gridview: true,
        rowattr: function(rd) {
            if (rd.done == 1) {
                return {"style": "background-color:green"};
            }
        },
        viewrecords: true,
        sortorder: "desc",
        gridComplete: function() {
            var ids = jQuery("#list").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                se = "<input style='height:22px;width:50px;' type='button' value='Edit' onclick=\"jQuery('#list').editGridRow('" + cl + "',{afterSubmit:function(t, data,postdata, frmoper){return saveTask(t, data,postdata, frmoper);}});\" />";
                ce = "<input style='height:22px;width:50px;' type='button' value='Delete' onclick=\"jQuery('#list').delGridRow('" + cl + "',{afterSubmit:function(t, data,postdata, frmoper){return deleteTask(t, data,postdata, frmoper);}});\" />";
                jQuery("#list").jqGrid('setRowData', ids[i], {save: se, delete: ce});
            }
        },
        editurl: "clientArray", caption: "Input Types", cellsubmit: "clientArray",
    }
    );
    for (var i = 0; i < tasks.length; i++) {
        var task = tasks[i];
        //var datad = [ {title: task.title,description:"2007-10-01",runtTill:"test",doneinPercent:"note",done:"200.00"}];
        jQuery("#list").jqGrid('addRowData', task.Taskid, task);
    }
}

function deleteTask(t, data, postdata, frmoper) {

    deleteTaskFromStorage(data.id);
    //close modal window 
    $(".ui-icon-closethick").trigger('click');
    return true;
}
function getTaskById(id) {

    var tasks = getTasksFromStorage();
    if (tasks) {
        var index = getTaskIndexFromId(id, tasks);
        return tasks[index];
    }
    return null;
}
function getTaskIndexFromId(Taskid, tasks) {
    for (var i = 0; i < tasks.length; i++) {
        var task = tasks[i];
        if (task.Taskid == Taskid)
            return i;
    }
}
function saveTask(t, data, postdata, frmoper) {

    deleteTaskFromStorage(data.id);
    appendToLocalStorage(data);
    listTasks();
    //close modal window 
    $(".ui-icon-closethick").trigger('click');
    return true;
}
function loadPage(url){
    $('#content').load(url);
    Gumby.init();
}
